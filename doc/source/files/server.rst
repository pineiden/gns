.. _server:

Server System
=============

The system provides a server who allow connect with multiple clients. That clients
are identified by the assigned role  by the administrator, this allow the execution of
a certain command group. Uo can see this in :ref:`models`.

The server listen string messages. That could be:

#. message transference betwen clients, identified by a session code, 
#. obtain the list of clients 
#. the avalaible command group.

The GNS server recognize the allowed commands in the string messages who 
send the clients, generate appropriate channeling (routing) and register 
whit a logging system actions taken and results.

It can be daemonize to run in background and listen for clients.


.. figure:: img/server.svg
   :width: 100 %


.. automodule:: server
    :members:
    
    
.. toctree::
   :maxdepth: 2
   
   glosario
