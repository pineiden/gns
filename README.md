GPS Network Server [GNS]
=============================

The GNS system allow communicate different clients using a SSH protocol interface (secure and robust).

Has a database (sqlite) and a model system (orm in sqlalchemy). Identify users and register logs.

Can identfy a message like a:

* Login
* Command
* Request
* Message

In the standar system there are 3 level groups:

* Admin
* Monitor
* Slave

Every user has a group, an user whithout identification (sending password to GNS)only can send messages tho another clients.

Folder Structure
-------------------

The folder structure give us an idea about how the GNS System is developed. 
Run `tree` with some options and the see the list.

`tree -d -I __pycache__`

Now <agosto 26, 2016> the list has documentation, images, log, org-mode, orm and test folders.

```
.
├── doc
│   ├── build
│   │   ├── doctrees
│   │   │   └── files
│   │   ├── files
│   │   │   └── files
│   │   │       └── img
│   │   ├── html
│   │   │   ├── files
│   │   │   │   └── files
│   │   │   │       └── img
│   │   │   ├── _images
│   │   │   │   └── math
│   │   │   ├── _modules
│   │   │   ├── _sources
│   │   │   │   └── files
│   │   │   └── _static
│   │   ├── _sources
│   │   │   ├── files
│   │   │   └── rst
│   │   └── _static
│   └── source
│       ├── files
│       ├── img
│       ├── nsstatic
│       └── nstemplates
├── img
├── log
├── org
├── orm
│   └── data
└── test
```

Documentation
-------------

The project documentation is made with the python Sphinx tool, and give us a pdf and html reference in folder `doc`.

Here, in the Sphinx documentation, you can see the classes and methods, the algorithms developed for the GNS system.

For a better documentation, when write in the Sphinx system it's recommended use 'sphinx-autobuild', this module reload in real time the browser.

## Base Technology

GNS system run under GNU/Linux or Unix, it use the bash features.

The GNS system use the latest python version 3.5. This version has a base module for multithread work: `asyncio` [https://docs.python.org/3/library/asyncio.html](see documentation).

### Install Gnu/Linux

It is advisable use a Debian base distribution or a Red Hat distribution, also Suse.

### Install Python 3.5

You can install from repositories or compiled

1. [https://launchpad.net/~fkrull/+archive/ubuntu/deadsnakes] (PPA de ubuntu)

2. Compiling first dependences:

`apt-get build-dep python3` and

```{engine='sh'}
sudo apt-get install build-essential
sudo apt-get install libz-dev
sudo apt-get install libreadline-dev
sudo apt-get install libncursesw5-dev
sudo apt-get install libssl-dev
sudo apt-get install libgdbm-dev
sudo apt-get install libsqlite3-dev
sudo apt-get install libbz2-dev
sudo apt-get install liblzma-dev
sudo apt-get install tk-dev
sudo apt-get install libdb-dev
sudo apt-get install libc6-dev
sudo apt-get build-dep python3
sudo apt-get install openssl-dev
sudo apt-get install openssl
sudo apt-get install libssl-dev
sudo apt-get install zlib1g-dev
sudo apt-get install zlib1g
```

And then download [https://www.python.org/downloads/](Python 3.5) and compile:

```{engine='sh'}
./configure
make -j $(nproc)
sudo make install
```

### Install pip

`sudo apt-get install python3.5-pip`

### Install virtualenv and virtualenvwrapper

Read about [https://virtualenv.pypa.io/en/stable/](virtualenv) and the wrapper
[https://virtualenvwrapper.readthedocs.io/en/latest/](virtualenvwrapper). It's very useful for the python projects.

`sudo pip3.5 install virtualenv virtualenvwrapper`

Add the settings variables on your `.profile` or `.bashrc` file (`/home/user/.the_file`). It runs every time open a terminal and allow some setted commands.

```
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Proyectos
export VIRTUALENV_PYTHON=/usr/bin/python3.5
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
source /usr/local/bin/virtualenvwrapper.sh

```
NOTE: Check the correct path for python 3.5

### Create virtual environment

`makevirtualenv gns`

### Install requeriments

In virtual environment `gns`, type `workon gns`.

`pip install -r requeriments.txt`

That command install all modules required for work with the GNS system.

## An important module `asyncssh`

The `asyncssh` module it's the main parent technology in what the GNS system it's implemented.

You can get the code from [https://github.com/ronf/asyncssh](Github) and see the documentation in [http://asyncssh.readthedocs.io/en/stable/#](Readthedocs).

The system allow Asynchronous SSH sessions, and have some general clases for Servers and Clients. Uses the OpenSSH features, and support SFTP. 

Allow multiple and simoultaneos sessions with a single connection.

Can use password, public_key and anothers.

## The database Schema

The general gns schema has two areas:

* User manage
* Log manage

The schema diagram:

![GNS Schema](/orm/gns_schema.png)

It allow manage a system to manage and validate users, register the activity on the log tables.

## Load sample data

There are a folder in `orm/data` in what there are some files, a file by table with data.

To load this data, first you can add more in the same structure (don't change headers), run the `orm/load_data.py` script.

This generate the database with the sample test data.

## Then run the server

Run the server, when all before it's ok, is simple. In the `gns` virtualenv, parent folder:

`python run.py`

If you have problems with the access to ssh keys, with sudo:

`sudo ~/.virtualenvs/networkserver/bin/python ./run.py`

## And connect test clients

This it's the funny part.

We have some goals:

* Connect a session
* Synchronice and give PASSWORD
* Get command list
* Get session list
* Send messages
* Send commands
* Send request

How to do that?

An easy way is using 2 terminals:

1. First terminal you have to `echo` the message to a file
2. Second terminal you extract the message from file and connect the ssh session

In advance, the file could be also a socket (but it's a litle more complicated)

When connect to server, it sends you a message with an unique identificator: IDX. That code
it's for you and your session, it have to be in every message to identify your message with your user.

### LOGIN

For login you give 2 messages, one for sync the user, and the second for identify with the password. If you only do firts, the user only can send messages.

## Future and Posible Projects

0. More documentation
1. Daemonize the GNS system
2. Chat with GNS
2. Email with GNS
3. Add, drop users and new commands in GNS -> Manage this commands in GNC
4. Check need for certain methods
