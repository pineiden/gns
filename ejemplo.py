
def sample_disk(points, radius=1.0, coords='cartesian'):
    r"""
    Samples the disk :math:`\overline{D}=\{(x, y) \in \mathbb{R}^2
    \colon \, x^2+y^2 \leq r^2\}` using the function :py:mod:`sunflower`.

    The polar coordinates of the :math:`k`-th point are

    .. math::
        r_k = r\sqrt{\frac{k}{n}}

        \varphi_k = \psi_0 k

    :param points: number of samples :math:`n`.
    :param radius: radius of the disk :math:`r`.
    :param coords: coordinates of the output. 'cartesian' or 'polar'.
    :type points: int
    :type radius: float
    :type coords: str
    :return: either :math:`\begin{bmatrix}x_1 & \dots & x_n \\ y_1 & \dots & y_n
        \end{bmatrix}` or :math:`\begin{bmatrix}r_1 & \dots & r_n \\ \varphi_1 &
        \dots & \varphi_n\end{bmatrix}` depending on the *coords* parameter.
    :rtype: numpy.ndarray [2][]

    .. raw:: html

        <div class="clearer"></div>
    """
    return sunflower(points, c=radius/mat.sqrt(points), coords=coords)


def set_ivars(obj, var_names=None, values=None, **kw_ivars):
    """
    Sets values to a set of instance variables in a class.

    :param obj: instance of a an arbitrary class.
    :param var_names: names of the instance variables (strings).
    :param values: values of these variables.
    :param kw__i_vars: key-worded instance variables.

    .. warning:: this function does simple assignments, and thus objects are
        not copied; a simple reference to the new object is created.

    .. highlight:: python

    Example:

    .. code-block:: python
        :linenos:
        :emphasize-lines: 15, 18, 22

        >>> message_stencil = 'length is {:.6f};  width is {:.6f}'
        >>>
        >>> class Rectangle:
        >>>     def __init__(self, length=1., width=1.):
        >>>         self.length = length
        >>>         self.width = width
        >>>
        >>>     def print_vars(self):
        >>>         # print length and width
        >>>         print message_stencil.format(self.length, self.width)
        >>>
        >>> rect = Rectangle()
        >>> rect.print_vars()
        length is 1.000000;  width is 1.000000
        >>> set_ivars(rect, ('length', 'width'), (2.0, 3.0))
        >>> rect.print_vars()
        length is 2.000000;  width is 3.000000
        >>> set_ivars(rect, length=2.5, width=0.5)
        >>> set_ivars(rect, length=2.5, width=0.5)
        >>> rect.print_vars()
        length is 2.500000;  width is 0.500000
        >>> dict_vars = {'length': 0.9, 'width': 6.1}
        >>> set_ivars(rect, **dict_vars)
        >>> rect.print_vars()
        length is 0.900000;  width is 6.100000
        >>> set_ivars(rect, ('length', 'width'), 3.141592)
        >>> rect.print_vars()
        length is 3.141592;  width is 3.141592
        >>> set_ivars(rect, ('length', 'width'))
        >>> print (rect.length, rect.width)
        (None, None)
    """
    values_tup = isinstance(values, (list, tuple))
    keys_tup = isinstance(var_names, (list, tuple))
    if kw_ivars in [{}, None]:
        if keys_tup:
            n_keys = len(var_names)
            if n_keys < 1:
                return
            if values_tup:
                if (set(var_names).issubset(set(obj.__dict__.keys())) and
                            n_keys == len(values)):
                    obj.__dict__.update(zip(var_names, values))
            else:
                obj.__dict__.update(zip(var_names, [values]*n_keys))
        else:
            if 'var_names' in obj.__dict__.keys() and var_names is not None:
                obj.__dict__['var_names'] = var_names
            if ('values' in obj.__dict__.keys() and not values_tup and
                        values is not None):
                obj.__dict__['values'] = values
    else:
        if ('values' in obj.__dict__.keys() and not values_tup and
                    values is not None):
            obj.__dict__['values'] = values
        if ('var_names' in obj.__dict__.keys() and not keys_tup and
                    var_names is not None):
            obj.__dict__['var_names'] = var_names
        if isinstance(kw_ivars, dict):
            if (None not in kw_ivars.values()) and set(
                    kw_ivars.keys()).issubset(set(obj.__dict__.keys())):
                obj.__dict__.update(kw_ivars)