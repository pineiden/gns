"""
There are constant values to define how to run the GNS Server
"""
PORT = 9988
HOST_KEYS = '/etc/ssh/ssh_host_dsa_key'
CLIENT_KEYS = '/home/david/.ssh/authorized_keys'
LIST_COMMANDS = dict(
    INFO=dict(
        desc="Information from server",
        code="INFO|[IDX]|[MSG_DATA]|[CLIENTID]",
        group=['all', 'admin', 'monitor', 'slave']),
    SYNCCLIENT=dict(
        desc="Se sincroniza id session con id de cliente",
        code="SYNCCLIENT|[IDX]|ID|[CLIENTID]",
        group=['all', 'admin', 'monitor', 'slave']),
    CLNTPSSWD=dict(
        desc="Se envía a servidor password de cliente para asignarle, si \
            corresponde, el nivel de usuario",
        code="CLNTPSSWD|[IDX]|PW|[PSSWD]|[CLIENTID]",
        group=['all', 'admin', 'monitor', 'slave']),
    SESSIONLST=dict(
        desc="Se envía lista de sesiones activas en servidor a cliente",
        code='SESSIONLST|[IDX]|GET|LST|[CLIENTID]',
        group=['admin', 'monitor']),
    COMMANDLST=dict(
        desc="Se envía la lista de comandos que soporta servidor en \
            atención a cliente",
        code='COMMANDLST|[IDX]|GET|LST|CMD|[CLIENTID]',
        group=['admin', 'monitor', 'slave']),
    SENDMSG1=dict(
        desc="Se envía mensaje a un solo destinatario",
        code='SENDMSG1|[IDX]|SND|MSG|UNIQ|[CLIENTID]|[DESTID]|[MSG_DATA]',
        group=['admin', 'monitor', 'slave']),
    SENDMSGALL=dict(
        desc="Se envía mensaje a todos",
        code='SENDMSGALL|[IDX]|SND|MSG|ALL|[CLIENTID]|[MSG_DATA]',
        group=['admin', 'monitor']),
    SENDMSGGRP=dict(
        desc="Se envía mensajes a grupo seleccionado",
        code='SENDMSGGRP|[IDX]|SND|MSG|GRP|[CLIENTID]|[GRP_LST]|[MSG_DATA]',
        group=['admin', 'monitor', 'slave']),
    CLOSECNN=dict(
        desc="Close a session",
        code='CLOSECNN|[IDX]|CLOSE|[CLIENTID]',
        group=['admin']))

for k in LIST_COMMANDS:
    LIST_COMMANDS[k]['answer'] = LIST_COMMANDS[k]['code'].replace("[IDX]",
                                                                  'ANSWER')

LIST_LOGS = dict(
    set_uin=dict(
        msg="Define IDX length",
        type='info', ),
    snd_id_ok=dict(
        msg='Request for client id sended',
        type='info', ),
    snd_id_nok=dict(
        msg='Request for client id cannot sended',
        type='error', ),
    pass_cl=dict(
        msg="Password identificada correctamente", type='info'),
    no_pass_cl=dict(
        msg="Password no identificada correctamente", type='error'),
    wc_ok=dict(
        msg='Welcome sended',
        type='info', ),
    wc_nok=dict(
        msg='Welcome cannot sended',
        type='error', ),
    sync_cl=dict(
        msg='Synchronization with client ok',
        type='info', ),
    no_sync_cl=dict(
        msg='Synchronization with client not ok',
        type='error', ),
    session_lst=dict(
        msg='Request for session list attended',
        type='info', ),
    no_session_lst=dict(
        msg="No session list sended", type='error'),
    command_lst=dict(
        msg='Request for command list attended',
        type='info', ),
    no_command_lst=dict(
        msg="No command list sended", type='error'),
    snd_msg_ok=dict(
        msg='Send message to another 1 client ok',
        type='info', ),
    snd_msg_reject=dict(
        msg="Rejected, user doesn't exist", type='warning'),
    snd_msg_nok=dict(
        msg='Send message to another 1 client not ok',
        type='error', ),
    snd_all_ok_id=dict(
        msg='Send message to all clients ok',
        type='info', ),
    snd_all_nok_id=dict(
        msg='Send message to all clients not ok',
        type='error', ),
    snd_grp_ok_id=dict(
        msg='Send message to group of clients ok',
        type='info', ),
    snd_grp_nok_id=dict(
        msg='Send message to group of clients not ok',
        type='error', ),
    session_close=dict(
        msg='Close a session', type='info'),
    client_added=dict(
        msg="Client added to session", type='info'),
    client_not_added=dict(
        msg="Client cannot added", type='error'))
separator = '|'
m_end = '<end>'
