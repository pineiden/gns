def isPositive(value):
    """
    Validate if value is positive integer
    :param value:
    :return:
    """
    ip = False
    if value >= 0:
        ip = True
    return ip