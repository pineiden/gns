import asyncio
import asyncssh
import sys
import os
try:
    from .post_server import GNS
except Exception:
    from post_server import GNS
try:
    from .conf.settings import PORT, HOST_KEYS, CLIENT_KEYS
except Exception:
    from conf.settings import PORT, HOST_KEYS, CLIENT_KEYS

async def start_server():
    """
    That method run a server to atend clients, using asyncio coroutines
    :return:
    """
    await asyncssh.create_server(GNS, '', PORT,
                                 server_host_keys=HOST_KEYS,
                                 authorized_client_keys=CLIENT_KEYS)
loop = asyncio.get_event_loop()

try:
    """
    Run server in a loop
    """
    loop.run_until_complete(start_server())
except (OSError, asyncssh.Error) as exc:
    sys.exit('Error starting server: ' + str(exc))

loop.run_forever()
