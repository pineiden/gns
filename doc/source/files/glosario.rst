.. _glossary:

Glossary
========

.. glossary::
    :sorted:


    IDX
        session ID

    SND
        send

    ID
        string ID
        
    CLIENTID
        client ID

    LST
        list
        
    GET
        string *get*
        
    
