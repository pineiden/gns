# enable asyncio items and systems
import asyncio
import asyncssh
import logging
import datetime
import json
try:
    from orm.db_session import session
    from orm.models import Level, User, Command, Alert, Message, Log
    from orm.db_users import SessionGNS
    from orm.password import PasswordHash
except Exception:
    from .orm.db_session import session
    from .orm.models import Level, User, Command, Alert, Message, Log
    from .orm.db_users import SessionGNS
    from .orm.password import PasswordHash

try:
    from conf.settings import LIST_COMMANDS, LIST_LOGS, separator, m_end
except Exception:
    from .conf.settings import LIST_COMMANDS, LIST_LOGS, separator, m_end
"""
Este método es para generar un identificador único para distintas instancias
durante el tiempo de operación del servidor
"""
try:
    from library import my_random_string, fill_pattern,\
        pattern_value, context_split
except Exception:
    from .library import my_random_string, fill_pattern,\
        pattern_value, context_split


class GPSNetworkServerSession(asyncssh.SSHServerSession):
    """A class to manage session from a client connection.
    It define an ID by session: IDX
    .. note::
        [X] parenthesis mean a variable value of X

    When a client it's connected send a string  <IDX>|SND|ID to it,
    in what the client has to synchronice the session. The client must take IDX
    and send to server its *client id*:

    * **msg** [IDX]|ID|[CLIENTID].

    The client can ask or make a query about the another clients, getting the
    list:

    * **msg** [IDX]|GET|LST|[CLIENTID], the server return dictionary (hash)
        with the information

    The others commands could be obtained asking about COMMAND dict.

    * **msg** [IDX]|GET|LST|CMD|[CLIENTID].

    And to end a message to some client:

    * **1:1** -> [IDX]|SND|MSG|UNIQ|CLID|MYID|MSG_DATA

    * **1:all** -> [IDX]|SND|MSG|ALL|[CLIENTID]|[MSG_DATA]

    * **1:n<=all** -> [IDX]|SND|MSG|GRP|[CLID]|[MSG_DATA]

    """
    SESSION = {}
    ANNON_CHANNEL = {}
    CHANNEL = {}
    COMMANDS = LIST_COMMANDS
    log_file = 'log/ssh_session.log'
    logging.basicConfig(filename=log_file, level=logging.DEBUG)
    LOG_MSG = LIST_LOGS
    # identificador de session:
    # Debe contener una lista de sesiones
    idx = []
    m_end = m_end

    def __init__(self, uin=6):
        """
        Init:

        :param uin: session identifier length
        """
        print("Se inicia GNS Server")
        self.db_session = SessionGNS()
        self.gns_method = 'init'
        self._input = ''
        self._total = 0
        self.uin = uin
        IDX = my_random_string(self.uin)
        self.idx = IDX
        self.group = 'all'
        self.client_id = 'ANNON'
        # print (IDX)
        t = True
        while t:
            if IDX not in type(self).idx:
                type(self).idx.append(IDX)
                t = False
            else:
                IDX = my_random_string(self.uin)
        print(self.idx)
        # -> addlog largo id
        msg_key = 'set_uin'
        # info Esto
        # está bien si y solo si en la creacion de nuevas instancias no se
        # modifica el uin de otra manera sería necesario reiniciar servidor
        print("Calculando tamaños")
        for k in GPSNetworkServerSession.COMMANDS.keys():
            # print(k)
            c_length = len(k)
            plecas = GPSNetworkServerSession.COMMANDS[k]['code'].count(
                separator)
            if k == 'SYNCCLIENT':
                c_length += self.uin + plecas + 2  # ID
            elif k == 'CLNTPSSWD':
                c_length += self.uin + plecas + 2  # PW
            elif k == 'INFO':
                c_length += self.uin + plecas + 0  # no names
            elif k == 'SESSIONLST':
                c_length += self.uin + plecas + 3 + 3  # GEt+LST
            elif k == 'COMMANDLST':
                c_length += self.uin + plecas + 3 + 3 + 3  # GET+LST+CMD
            elif k == 'SENDMSG1':
                c_length += self.uin + plecas + 3 + 3 + 4  # SND+MSG+UNIQ
            elif k == 'SENDMSGALL':
                c_length += self.uin + plecas + 3 + 3 + 3  # SND+MSG+ALL
            elif k == 'SENDMSGGRP':
                c_length += self.uin + plecas + 3 + 3 + 3  # SND+MSG+GRP
            GPSNetworkServerSession.COMMANDS[k]['c_length'] = c_length
        # print("End-init")
        print(self.gns_method + " " + self.idx + " " + msg_key + " " + str(
            self.uin))
        try:
            print(self.gns_method)
            print(self.idx)
            print(msg_key)
            print(str(self.uin))
            GPSNetworkServerSession.addLog(self.gns_method, self.idx, msg_key,
                                           str(self.uin))
        except Exception as exec:
            print("No se pudo registrar en log inicio de sesión")
            print(exec)

    def connection_made(self, chan):
        """
        It run when the connection it's ok.
        It allow to send a msg to client asking the id

        :param chan: A Channel
        :return:
        """
        # called when channel is openned sucessfully SSHClientChannel
        print("Connection made")
        print(chan.get_extra_info('peername'))
        print(chan.get_write_buffer_size())
        try:
            chan.info_msg("\"Conexión exitosa\"")
        except:
            print("No se envio exito conexion")
        method = 'connection_made'
        self._chan = chan
        GPSNetworkServerSession.ANNON_CHANNEL.update({self.idx: chan})

        # print (self._chan.get_extra_info ('connection'))
        # print (self._chan.get_extra_info ('local_peername'))
        # print (self._chan.get_extra_info ('remote_peername'))
        # print (self._chan.get_extra_info ('connection').get_extra_info (
        #    'send_mac'))
        # rint(self._chan)
        print("Serversession")
        # id_cl = len(GPSNetworkServerSession.clients())

        (ip, port) = chan.get_extra_info('peername')
        self.ip = ip
        self.port = port
        IDX = self.idx
        msg = "\"Tu ip es: " + ip + " " + self.client_id + "\""
        print(chan)
        print(chan.get_extra_info('peername'))
        print("First msg " + msg)
        # GPSNetworkServerSession.info_msg(IDX, self.client_id, msg)
        msg = "\"INFO|" + IDX + "|SND|ID \""
        print("Test")
        print(IDX + ":" + self.idx)
        print(self.client_id)
        try:
            # chan.write("Hola\r\n")
            # chan.write(msg + "\r\n")
            GPSNetworkServerSession.info_msg(IDX, self.client_id, msg)
            msg_key = 'snd_id_ok'
        except:
            print("Info connection made with error")
            msg_key = 'snd_id_nok'
        print("Connection made done")
        print(chan.get_extra_info('peername'))
        print("END CONNECTION MADE")
        # chan.write("Hola\r\n")
        GPSNetworkServerSession.addLog(method, self.idx, msg_key, msg)  # info

    def shell_requested(self):
        """
        Shell enabled
        :return:
        """
        return True

    def exec_requested(self, command):
        """run SSH command"""
        return True

    def pty_requested(self, term_type, term_size, term_modes):
        return True

    def session_started(self):
        """
        Mensaje de inicio de sesion
        :return:
        """
        method = 'session_started'
        msg_key = ''
        try:
            print("Iniciando sessión")
            welcome = "\"Bienvenid@ al servidor GNS\""
            GPSNetworkServerSession.info_msg(self.idx, self.client_id, welcome)
            print(welcome)
            GPSNetworkServerSession.info_msg(
                self.idx, self.client_id,
                "\"Obtén tus comandos: COMMANDLST | [IDX] | GET | LST | CMD |"
                + " [CLIENTID]\r\n Donde CLIENTID:ANNON\"")
            msg_key = 'wc_ok'
        except:
            msg_key = 'wc_nok'
        GPSNetworkServerSession.addLog(method, self.idx, msg_key,
                                       "Welcome")  # info

    def data_received(self, data, datatype):
        """
        Data proccesing, here the data can be send and received
        :param data: is a string or bytes received from a client
        :param datatype: not used in that case
        :return:
        """
        method = 'data_received'
        try:
            var_list = []
            self._input += data.strip()
            info = self._input.split('|')
            print(info)
            print("Recibiendo data:")
            print("Nombre de Comando:")
            print(info[0])
            print("Cliente Id: " + self.client_id)
            msg_type = info[0]
            info_pt = GPSNetworkServerSession.COMMANDS['INFO']['answer']
            print(info_pt)
            print("Intentando revisar msg")
            print(len(info))
        except Exception as exec:
            print("Data input error")
            print(exec)
        if len(info) > 1:
            print("Info mayor a 1")
            if msg_type in GPSNetworkServerSession.COMMANDS.keys() and info[
                    1] == self.idx:
                print(GPSNetworkServerSession.COMMANDS[msg_type])
                print(msg_type)
                c_length = GPSNetworkServerSession.COMMANDS[msg_type][
                    'c_length']
                pattern = GPSNetworkServerSession.COMMANDS[msg_type]['answer']
                print(c_length)
                if info[1] == self.idx:
                    # SYNCCLIENT
                    # SYNCCLIENT|<IDX>|ID|<CLIENTID>
                    if info[0] == 'SYNCCLIENT' and info[2] == 'ID':
                        print("Se recibe ID")
                        self.client_id = info[3]
                        self.group = 'all'
                        self.client_pw = ''
                        # From database
                        self.user = User()
                        print(self.client_id)
                        try:
                            print(GPSNetworkServerSession.SESSION)
                            var_list.append(
                                pattern_value("[CLIENTID]", json.dumps(
                                    GPSNetworkServerSession.SESSION)))
                            msg_send = fill_pattern(var_list, pattern)
                            self._chan.write(msg_send + "\r\n")
                            msg_key = "sync_cl"
                        except:
                            msg_key = "no_sync_cl"
                        print("Agregando sesión " + msg_key)
                        GPSNetworkServerSession.add_session(
                            self.idx, self.client_id, self.group, self._chan,
                            self.ip, self.port)
                        print("Pasando registro a log")
                        GPSNetworkServerSession.addLog(method, self.idx,
                                                       msg_key, self.client_id)

                    # CLNTPSSWD
                    # CLNTPSSWD|[IDX]|PW|[PSSWD]
                    elif info[0] == 'CLNTPSSWD' and info[2] == 'PW':
                        print("Se recibe Password")
                        self.client_pw = info[3]
                        client_id = info[4]
                        print(self.client_id)
                        assert client_id == self.client_id, "No es el mismo"\
                            + " id de cliente"
                        print(self.client_pw)
                        print("Verificando password a método")
                        (self.verify,
                         self.user) = self.db_session.verify_password(
                             self.client_id, self.client_pw)
                        print("Usuario ok")
                        # OK print(self.user.level_id)
                        self.level = self.db_session.get_level_by_id(
                            self.user.level_id)
                        # OK print(self.level)
                        var_list.append(
                            pattern_value("[CLIENTID]", self.client_id))
                        var_list.append(
                            pattern_value("[PSWD]", "OK:GROUP:" +
                                          self.level.group))
                        try:
                            msg_send = fill_pattern(var_list, pattern)
                            self._chan.write(msg_send + "\r\n")
                            msg_key = "pass_cl"
                        except:
                            msg_key = "no_pass_cl"
                        # OK print("Agregando password a sesión")
                        # Agregar nivel de usuario a SESSION
                        self.group = self.level.group
                        GPSNetworkServerSession.SESSION[self.idx][
                            'group'] = self.group
                        print("Msj es:" + msg_key)
                        GPSNetworkServerSession.addLog(method, self.idx,
                                                       msg_key, self.client_id)

                    # SESSIONLST
                    # SESSIONLST|IDX|GET|LST|ID

                    elif info[0] == 'SESSIONLST' and info[2] == 'GET' and info[
                            4] == self.client_id:
                        if info[3] == 'LST':
                            print("enviando lista")
                            print(GPSNetworkServerSession.SESSION)
                            pattern.replace('GET', 'RCV')
                            var_list.append(
                                pattern_value('[CLIENTID]', json.dumps(
                                    GPSNetworkServerSession.SESSION)))
                            msg_send = fill_pattern(var_list, pattern)
                            # Verificar pertentencia a grupo:
                            msg_key = ''
                            if self.group in type(self).COMMANDS[info[0]][
                                    'group']:
                                GPSNetworkServerSession.send_info(self.idx,
                                                                  msg_send)
                                msg_key = 'session_lst'
                            else:
                                msg = 'Verificia tu ID '\
                                    + 'de cliente enviando '\
                                    + 'passwd: [IDX]|PW|[PSSWD]'
                                type(self).info_msg(self.idx, self.client_id,
                                                    msg)
                                msg_key = 'no_session_lst'
                            GPSNetworkServerSession.addLog(
                                method, self.idx, msg_key, self.client_id)
                    # COMMANDLST
                    # COMMANDLST|IDX|GET|LST|CMD|ID

                    elif info[0] == 'COMMANDLST' and info[2] == 'GET' and info[
                            5] == self.client_id:
                        if info[3] == 'LST' and info[4] == 'CMD':
                            this_commands = type(self).commands_by_group(
                                self.group)
                            pattern.replace('GET', 'RCV')
                            var_list.append(
                                pattern_value('[CLIENTID]', json.dumps(
                                    this_commands)))
                            msg_send = fill_pattern(var_list, pattern)
                            if self.group in this_commands[info[0]]['group']:
                                msg_key = 'command_lst'
                                type(self).send_info(self.idx, msg_send)
                            else:
                                msg = '\"Verificia tu ID '\
                                    + 'de cliente enviando '\
                                    + 'passwd: [IDX]|PW|[PSSWD]\"'
                                type(self).info_msg(self.idx, self.client_id,
                                                    msg)
                                msg_key = 'no_command_lst'
                            GPSNetworkServerSession.addLog(
                                method, self.idx, msg_key, self.client_id)
                    # SENDMSG1
                    # SENDMSG1|IDX|SND|MSG|UNIQ|[CLID]|[DESTID]|MSG_DATA
                    # MSG_DATA="VARNAME|DATA"

                    elif info[0] == 'SENDMSG1' and info[2] == 'SND' and info[
                            3] == 'MSG' and info[4] == 'UNIQ' and info[
                                5] == self.client_id:
                        # verificar si info[4] aún está disponible
                        msg_key = ''
                        try:
                            # Buscar en directorio el idx o key del elemento con
                            # client_id
                            dest_id = info[6]
                            # Check assert if dest_id is in list

                            tot_length = c_length + \
                                len(dest_id) + len(info[5])
                            msg_data = self._input[tot_length::]
                            # GET DESTINY IDX (if existe give)
                            idx = GPSNetworkServerSession.get_idx(dest_id)
                            if idx != 'NONE':
                                # Create msg
                                pattern.replace('SND', 'RCV')
                                var_list.append(
                                    pattern_value('[MSG_DATA]', msg_data))
                                var_list.append(
                                    pattern_value('[DESTID]', dest_id))
                                var_list.append(
                                    pattern_value("[CLIENTID]",
                                                  self.client_id))
                                msg_send = fill_pattern(var_list, info_pt)
                                msg_key = 'snd_msg_ok'
                                GPSNetworkServerSession.send_info(idx,
                                                                  msg_send)
                            else:
                                pattern.replace('SND', 'RCV')
                                msg = "\"Rechazado, no existe usuario\""
                                var_list.append(
                                    pattern_value('[MSG_DATA]', msg))
                                var_list.append(
                                    pattern_value('[DESTID]', dest_id))
                                var_list.append(
                                    pattern_value("[CLIENTID]",
                                                  self.client_id))
                                msg_send = fill_pattern(var_list, info_pt)
                                msg_key = 'snd_msg_reject'
                                GPSNetworkServerSession.send_info(self.idx,
                                                                  msg_send)

                        except:
                            msg = '\"Ya no está conectado\"'
                            type(self).info_msg(self.idx, self.client_id, msg)
                            msg_key = 'snd_msg_nok'
                        GPSNetworkServerSession.addLog(method, self.idx,
                                                       msg_key, self.client_id)
                    # SENDMSGALL
                    # IDX|SND|MSG|ALL|CLID|MSG_DATA

                    elif info[0] == 'SENDMSGALL' and info[2] == 'SND' and info[
                            3] == 'MSG' and info[4] == 'ALL' and info[
                                5] == self.client_id:
                        # verificar si info[5] aún está disponible
                        # Una lectura a la lista de sessiones y enviar por
                        # channel
                        tot_length = c_length + len(info[5])
                        msg_data = self._input[tot_length::]
                        msg_key = ''
                        pattern.replace('SND', 'RCV')
                        for k in GPSNetworkServerSession.SESSION.keys():
                            k_pattern = pattern
                            try:
                                # Crear msg: IDX RCV MSG DATA
                                idx = k
                                var_list.append(
                                    pattern_value('[MSG_DATA]', msg_data))
                                var_list.append(
                                    pattern_value("[CLIENTID]",
                                                  self.client_id))
                                msg_send = fill_pattern(var_list, k_pattern)
                                GPSNetworkServerSession.send_info(idx,
                                                                  msg_send)
                                msg_key = 'snd_all_ok_id'
                            except:
                                msg = '\"Ya no está conectado\"'
                                GPSNetworkServerSession.info_msg(
                                    self.idx, self.client_id, msg)
                                msg_key = 'snd_all_nok_id'
                            GPSNetworkServerSession.addLog(method, self.idx,
                                                           msg_key, k)
                    # SENDMSGGRP
                    # IDX|SND|MSG|GRP|CLID|MSG_DATA

                    elif info[0] == 'SENDMSGGRP' and info[2] == 'SND' and info[
                            3] == 'MSG' and info[4] == 'GRP' and info[
                                5] == self.client_id:
                        # verificar si info[4] aún está disponible
                        # Tengo una lista definida de estaciones a las que deseo
                        # enviar un msg
                        # Esta lista esta definida por un string a:b:c:d
                        s_list = info[6].split(":")
                        tot_length = c_length + len(info[5])
                        msg_data = self._input[tot_length::]
                        for s in s_list:
                            k_pattern = pattern
                            try:
                                client_id = s
                                idx = GPSNetworkServerSession.get_idx(
                                    client_id)
                                pattern.replace('SND', 'RCV')
                                if idx != 'NONE':
                                    var_list.append(
                                        pattern_value('[MSG_DATA]', msg_data))
                                    var_list.append(
                                        pattern_value("[CLIENTID]", client_id))
                                    var_list.append(
                                        pattern_value("[GRP_LST]", info[6]))
                                    msg_send = fill_pattern(var_list,
                                                            k_pattern)
                                    GPSNetworkServerSession.send_info(idx,
                                                                      msg_send)
                                    msg_key = 'snd_grp_ok_id'
                            except:
                                msg = '\"Ya no está conectado\"'
                                GPSNetworkServerSession.info_msg(
                                    self.idx, self.client_id, msg)
                                msg_key = 'snd_grp_nok_id'
                            GPSNetworkServerSession.addLog(method, self.idx,
                                                           msg_key, s)

                    elif info[0] == 'CLOSECNN' and self.group in type(
                            self).COMMANDS[info[0]]['group']:
                        idx = GPSNetworkServerSession.get_idx(client_id)
                        msg = '\"Cerrando conexión a solicitud de usuario admin\"'
                        GPSNetworkServerSession.info_msg(idx, self.client_id,
                                                         msg)
                        chann = type(self).CHANNEL[idx]
                        chann.close()
                        msg_key = 'session_close'
                        GPSNetworkServerSession.addLog(method, self.idx,
                                                       msg_key, idx)
                    else:
                        print(info)
                        msg = '\"Tu comando no se reconoce\"'
                        GPSNetworkServerSession.info_msg(idx, self.client_id,
                                                         msg)
                        GPSNetworkServerSession.addLog(method, self.idx,
                                                       'no_cmd', info)
        else:
            print(info)
            msg = '\"Tu mensaje no se reconoce\"'
            print(msg)
            print(self.idx)

            print([self.idx, self.client_id, msg])
            GPSNetworkServerSession.info_msg(self.idx, self.client_id, msg)
            # GPSNetworkServerSession.addLog(method, self.idx, 'no_cmd', info)

        self._input = ''

    def eof_received(self):
        """
        End of session
        :param self:
        :return:
        """
        self._chan.write('Total = %s\r\n' % self._total)
        self._chan.exit(0)

    def get_id(self):
        """
        Return client id
        :param self:
        :return:
        """
        return self.client_id

    def connection_lost(self, exc):
        """
        Erase chan and client from lists
        :param self:
        :param exc:
        :return:
        """
        method = 'connection_lost'
        print("Conexión perdida")
        print(self._chan)
        try:
            if self.client_id is not 'ANNON':
                del GPSNetworkServerSession.CHANNEL[self.idx]
            else:
                del GPSNetworkServerSession.ANNON_CHANNEL[self.idx]
        except:
            print("Ni siquiera conectó cliente")
        GPSNetworkServerSession.addLog(method, self.idx, "conn_lost",
                                       "Conexión perdida")  # info

    @staticmethod
    def get_idx(client_id):
        """
        Return IDX, session id, when the input it's a correct client id
        :param client_id:
        :return:
        """
        method = 'get_idx'
        W = GPSNetworkServerSession.SESSION
        idx = 'NONE'
        for k, c in W.items():
            if c['id_cliente'] == client_id:
                idx = k
                # podria ser mismo cliente con varias sesiones?
                # manejar lista idx:futuro
                break
        msg_key = "idx_fclient"
        GPSNetworkServerSession.addLog(method, idx, msg_key, client_id)  # info
        return idx

    @staticmethod
    def send_info(chan_id, info):
        """
        Send info using a channel
        :param chan_id:
        :param info:
        :return:
        """
        method = 'send_info'
        msg_key = ''
        # print ("Enviando" + info)
        try:
            print("Msg a canal:")
            print(GPSNetworkServerSession.ANNON_CHANNEL)
            print("Mensaje a enviar")
            print(info)
            print(info)
            print(chan_id)
            print(GPSNetworkServerSession.ANNON_CHANNEL.keys())
            if chan_id in GPSNetworkServerSession.ANNON_CHANNEL.keys():
                chan = GPSNetworkServerSession.ANNON_CHANNEL[chan_id]
            else:
                chan = GPSNetworkServerSession.CHANNEL[chan_id]

            print(chan.get_extra_info('peername'))
            if info is not '':
                print("To send; " + info + m_end)
                chan.write(info + m_end)
                print(chan.get_extra_info("peername"))
                print(info)
                print("SEND END")
            msg_key = 'snd_msg_ok'
        except Exception as exec:
            print(exec)
            print("Error en enviar información")
            msg_key = 'snd_msg_nok'
        try:
            print("Chan log:")
            print(chan.get_extra_info("peername"))
            # GPSNetworkServerSession.addLog(method, chan_id, msg_key,
            #"" info)  #
            msgj = "Log ok" + m_end

            chan.write(msgj)
            print(msgj)
            print(chan.get_extra_info("peername"))

        except Exception as exec:
            print("Error log")
            print(exec)

    @staticmethod
    def info_msg(idx, client_id, msg):
        """
        Send a info msg to idx session
        :param idx: session identificator (string)
        :param client_id: client identificator (string)
        :param  msg: message (string)
        """
        var_list = []
        msg_key = ''
        print("INFO MSG")
        # GPSNetworkServerSession.send_info(idx, "Recibe esto %%%")
        info = GPSNetworkServerSession.COMMANDS['INFO']['answer']
        var_list.append(pattern_value("[MSG_DATA]", msg))
        var_list.append(pattern_value("[CLIENTID]", client_id))
        msg_send = fill_pattern(var_list, info)
        print(idx)
        print("Enviando a terminal: " + msg_send)
        try:
            if msg_send is not '':
                print("To send_info")
                GPSNetworkServerSession.send_info(idx, msg_send)
                msg_key = 'info_sended'
        except Exception as exec:
            print(exec)
            print("Info no se pudo enviar desde info_msg")
            print("PRRPRPRP")
            msg_key = 'no_info_sended'

        gns_method = 'send_info'
        GPSNetworkServerSession.addLog(gns_method, client_id, msg_key,
                                       client_id)  # error

    @staticmethod
    def add_session(idx, id_cl, group, chan, ip, port):
        """
        Add session to the SESSION and CHANNEL dicts

        :param idx: session id assigned by server, string value
        :param id_cl: client id gived by client, string value
        :param chan: channel instance,
        :param ip: ip from client, string value
        :param port: port from client, int value
        :return:
        """
        client_id = idx
        gns_method = 'add_session'

        print(gns_method)

        client = dict(id_cliente=id_cl, group=group, ip=ip, port=port)
        msk_key = ''
        print(client)
        print("Añadir")
        try:
            GPSNetworkServerSession.ANNON_CHANNEL.pop(client_id)
            GPSNetworkServerSession.SESSION.update({client_id: client})
            GPSNetworkServerSession.CHANNEL.update({client_id: chan})
            msg_key = 'client_added'
        except:
            msg_key = 'client_not_added'
        print("Cliente añadido a sesion " + msg_key)
        client_list = "" + json.dumps(client)
        print(client_list)
        GPSNetworkServerSession.addLog(gns_method, client_id, msg_key,
                                       client_list)  # error

        print("Añadido")
        # print(GPSNetworkServerSession.sessions())

    @staticmethod
    def sessions():
        """
        Return sessions
        :return:
        """
        method = 'sessions'
        return GPSNetworkServerSession.SESSION

    @staticmethod
    def addLog(method, idx, msg_key, value):
        """
        A method to handle log in the session, the inputs are for
        indenty origin and values

        :param method: method name
        :param idx: session id
        :param msg_key: key to handle log
        :param value: value to log
        :return: write a file
        """
        # OK print("Haciendo el log")
        sep = '|'
        time = str(datetime.datetime.now())
        LOG = GPSNetworkServerSession.LOG_MSG
        # print(LOG)
        # OK print(LOG)
        # OK LOG = GPSNetworkServerSession.LOG_MSG
        # first string to log:
        # OKx3 print("Mensaje: ")
        # print(msg_key)
        # print(LOG[msg_key])
        log = time + sep + idx + sep + method + sep + \
            LOG[msg_key]['msg'] + sep + json.dumps(value)
        # OKx2 print("Imprime log")
        # print(log)
        #  print(LOG)
        try:
            if LOG[msg_key]['type'] == 'info':
                logging.info(log)
            elif LOG[msg_key]['type'] == 'error':
                logging.error(log)
        except:
            print("No se registró en LOG")

    @staticmethod
    def commands_by_group(group):
        """
        Returns the command list filtered by allowed group
        :param group: string name
        :return:
        """
        all_var = GPSNetworkServerSession.COMMANDS
        out_group = dict()
        for k in all_var.keys():
            if group in all_var[k]['group']:
                out_group.update({k: all_var[k]})
        return out_group

# Create alias Session
GNSs = GPSNetworkServerSession


class GPSNetworkServer(asyncssh.SSHServer):
    """It allow a system to attend clients, receive and send messages. The
    messages are recognized by its structure, that have commands inside to
    route or obtain some information. You can define a level permissions
    categories (groups) and assign to the users Some can do all commands,
    another can do a few commands The groups are defined on the database. The
    log messages, too. Use SSH protocol to establish a secure conedtion beetwen
    pairs

    """

    def session_requested(self):
        """
        Generate the session using GNSs

        :return:
        """
        self.session = GPSNetworkServerSession()
        return self.session

    def connection_made(self, connection):
        """
        When connection it's made, run code

        :param connection:
        :return:
        """
        self.connection = connection
        (ip, port) = connection.get_extra_info('peername')
        self.ip = ip
        self.port = port


GNS = GPSNetworkServer
