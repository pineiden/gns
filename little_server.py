import asyncio
import asyncssh
import logging
import datetime
import json

try:
    from orm.db_session import session
    from orm.models import Level, User, Command, Alert, Message, Log
    from orm.db_users import SessionGNS
    from orm.password import PasswordHash
except Exception:
    from .orm.db_session import session
    from .orm.models import Level, User, Command, Alert, Message, Log
    from .orm.db_users import SessionGNS
    from .orm.password import PasswordHash

try:
    from conf.settings import LIST_COMMANDS, LIST_LOGS, separator, m_end
except Exception:
    from .conf.settings import LIST_COMMANDS, LIST_LOGS, separator, m_end
"""
Este método es para generar un identificador único para distintas instancias
durante el tiempo de operación del servidor
"""
try:
    from library import my_random_string, fill_pattern,\
        pattern_value, context_split
except Exception:
    from .library import my_random_string, fill_pattern,\
        pattern_value, context_split


class LittleServerSession(asyncssh.SSHServerSession):
    SESSION = {}
    ANNON_CHANNEL = {}
    CHANNEL = {}
    COMMANDS = LIST_COMMANDS
    log_file = 'log/ssh_session.log'
    logging.basicConfig(filename=log_file, level=logging.DEBUG)
    LOG_MSG = LIST_LOGS
    # identificador de session:
    # Debe contener una lista de sesiones
    idx = []
    m_end = m_end

    CHAN = ''

    # modifica el uin de otra manera sería necesario reiniciar servidor

    def __init__(self, uin=6):
        """
        Init:

        :param uin: session identifier length
        """
        print("Se inicia GNS Server")
        self.db_session = SessionGNS()
        self.gns_method = 'init'
        self._input = ''
        self._total = 0
        self.uin = uin
        self.idx = self.set_idx(uin)
        self.group = 'all'
        self.client_id = 'ANNON'
        # print (IDX)
        print(self.idx)
        # -> addlog largo id
        msg_key = 'set_uin'
        # info Esto
        # está bien si y solo si en la creacion de nuevas instancias no se
        # modifica el uin de otra manera sería necesario reiniciar servidor
        print("Calculando tamaños")
        self.LSS = LittleServerSession
        for k in self.LSS.COMMANDS.keys():
            # print(k)
            c_length = len(k)
            plecas = self.LSS.COMMANDS[k]['code'].count(
                separator)
            if k == 'SYNCCLIENT':
                c_length += self.uin + plecas + 2  # ID
            elif k == 'CLNTPSSWD':
                c_length += self.uin + plecas + 2  # PW
            elif k == 'INFO':
                c_length += self.uin + plecas + 0  # no names
            elif k == 'SESSIONLST':
                c_length += self.uin + plecas + 3 + 3  # GEt+LST
            elif k == 'COMMANDLST':
                c_length += self.uin + plecas + 3 + 3 + 3  # GET+LST+CMD
            elif k == 'SENDMSG1':
                c_length += self.uin + plecas + 3 + 3 + 4  # SND+MSG+UNIQ
            elif k == 'SENDMSGALL':
                c_length += self.uin + plecas + 3 + 3 + 3  # SND+MSG+ALL
            elif k == 'SENDMSGGRP':
                c_length += self.uin + plecas + 3 + 3 + 3  # SND+MSG+GRP
            self.LSS.COMMANDS[k]['c_length'] = c_length
        # print("End-init")
        print(self.gns_method + " " + self.idx + " " + msg_key + " " + str(
            self.uin))

    def set_idx(self, uin):
        idx = my_random_string(uin)
        while True:
            if idx not in LittleServerSession.idx:
                LittleServerSession.idx.append(idx)
                break
            else:
                idx = my_random_string(self.uin)

        return idx

    def write(self, msg):
        print("Enviando " + msg)
        print(LittleServerSession.CHAN)
        if len(msg) > 0:
            print("Ok")
            self.chan.write(msg + "<end>")
        else:
            pass

    def read(self):
        msg = self._chan.read()
        return msg

    def connection_made(self, chan):
        """
        It run when the connection it's ok.
        It allow to send a msg to client asking the id

        :param chan: A Channel
        :return:
        """
        self.chan = chan
        # called when channel is openned sucessfully SSHClientChannel
        print("Connection made")
        (self.ip, self.port) = chan.get_extra_info('peername')
        print(chan.get_write_buffer_size())
        LittleServerSession.ANNON_CHANNEL.update({self.idx: chan})
        try:
            self.write("\"Conexión exitosa\"")
        except:
            print("No se envio exito conexion")

    def shell_requested(self):
        """
        Shell enabled
        :return:
        """
        return True

    def exec_requested(self, command):
        """run SSH command"""
        return True

    def pty_requested(self, term_type, term_size, term_modes):
        return True

    def session_started(self):
        """
        Mensaje de inicio de sesion
        :return:
        """
        method = 'session_started'
        msg_key = ''
        print("Iniciando sessión")
        welcome = "\"Bienvenid@ al servidor GNS\""

        # self.write(welcome)
        LittleServerSession.info_msg(
            self.idx, self.client_id, welcome)
        msg = "\"INFO|" + self.idx + "|SND|ID \""
        LittleServerSession.info_msg(self.idx, self.client_id, msg)
        info_msg = "\"Obtén tus comandos: COMMANDLST | [IDX] | GET | LST "\
                   + "| CMD | [CLIENTID]\r\n Donde CLIENTID:ANNON\""
        LittleServerSession.info_msg(self.idx, self.client_id, info_msg)
        print(welcome)

    def data_received(self, data, datatype):
        """
        Data proccesing, here the data can be send and received
        :param data: is a string or bytes received from a client
        :param datatype: not used in that case
        :return:
        """
        method = 'data_received'
        print("Recibiendo data:")
        print(data)
        recibido = "Recibido: " + data
        # elf.write(recibido)
        self.send_info(self.idx, data)
        self.manage_msg(data)

    def manage_msg(self, msg):
        var_list = []
        info = context_split(msg)
        # INFO pattern
        info_pt = LittleServerSession.COMMANDS['INFO']['answer']
        msg_type = info[0]
        if msg_type in LittleServerSession.COMMANDS.keys():
            if info[1] == self.idx:
                c_length = LittleServerSession.COMMANDS[msg_type]['c_length']
                pattern = LittleServerSession.COMMANDS[msg_type]['answer']
                print(c_length)
                # SYNCCLIENT
                # SYNCCLIENT|<IDX>|ID|<CLIENTID>
                if info[0] == 'SYNCCLIENT' and info[2] == 'ID':
                    print("Se recibe ID")
                    self.client_id = info[3]
                    self.group = 'all'
                    self.client_pw = ''
                    # From database
                    self.user = User()
                    print(self.client_id)
                    print("Agregando sesión ")
                    try:
                        print([self.idx, self.client_id, self.group, self.chan,
                               self.ip, self.port])
                        LittleServerSession.add_session(
                            self.idx, self.client_id, self.group, self.chan,
                            self.ip, self.port)
                    except Exception as exec:
                        print(exec)

                    try:
                        print(LittleServerSession.SESSION)
                        var_list.append(
                            pattern_value("[CLIENTID]", json.dumps(
                                LittleServerSession.SESSION)))
                        msg_send = fill_pattern(var_list, pattern)
                        LittleServerSession.send_info(self.idx, msg_send)
                        msg_key = "sync_cl"
                    except:
                        msg_key = "no_sync_cl"

                # CLNTPSSWD
                # CLNTPSSWD|[IDX]|PW|[PSSWD]
                #  in quotes
                elif info[0] == 'CLNTPSSWD' and info[2] == 'PW':
                    print("Se recibe Password")
                    print(info)
                    self.client_pw = info[3]
                    client_id = info[4]
                    print(self.client_id)
                    assert client_id == self.client_id, "No es el mismo"\
                        + " id de cliente"
                    print(self.client_pw)
                    print("Verificando password a método")
                    (self.verify,
                     self.user) = self.db_session.verify_password(
                         self.client_id, self.client_pw)
                    print("Usuario ok")
                    # OK print(self.user.level_id)
                    self.level = self.db_session.get_level_by_id(
                        self.user.level_id)
                    # OK print(self.level)
                    var_list.append(
                        pattern_value("[CLIENTID]", self.client_id))
                    var_list.append(
                        pattern_value("[PSWD]", "OK:GROUP:" +
                                      self.level.group))
                    try:
                        msg_send = fill_pattern(var_list, pattern)
                        self._chan.write(msg_send + "\r\n")
                        msg_key = "pass_cl"
                    except:
                        msg_key = "no_pass_cl"
                    # OK print("Agregando password a sesión")
                    # Agregar nivel de usuario a SESSION
                    self.group = self.level.group
                    LittleServerSession.SESSION[self.idx][
                        'group'] = self.group
                    LittleServerSession.info_msg(
                        self.idx, self.client_id, "Pass verificada")
                    print("Msj es:" + msg_key)

                # SESSIONLST
                # SESSIONLST|IDX|GET|LST|ID

                elif info[0] == 'SESSIONLST' and info[2] == 'GET' and info[
                        4] == self.client_id:
                    if info[3] == 'LST':
                        print("enviando lista")
                        print(LittleServerSession.SESSION)
                        pattern.replace('GET', 'RCV')
                        var_list.append(
                            pattern_value('[CLIENTID]', json.dumps(
                                LittleServerSession.SESSION)))
                        msg_send = fill_pattern(var_list, pattern)
                        # Verificar pertentencia a grupo:
                        msg_key = ''
                        LittleServerSession.send_info(self.idx,
                                                      msg_send)
                        msg_key = 'session_lst'

                # COMMANDLST
                # COMMANDLST|IDX|GET|LST|CMD|ID

                elif info[0] == 'COMMANDLST' and info[2] == 'GET' and info[
                        5] == self.client_id:
                    if info[3] == 'LST' and info[4] == 'CMD':
                        this_commands = LittleServerSession.commands_by_group(
                            self.group)
                        pattern.replace('GET', 'RCV')
                        var_list.append(
                            pattern_value('[CLIENTID]', json.dumps(
                                this_commands)))
                        msg_send = fill_pattern(var_list, pattern)
                        print("Mensaje a enviar a")
                        print(self.group)
                        msg_key = 'command_lst'
                        print(msg_key)
                        LittleServerSession.send_info(self.idx, msg_send)
                        msg_key = 'no_command_lst'

                # SENDMSG1
                # SENDMSG1|IDX|SND|MSG|UNIQ|[CLID]|[DESTID]|MSG_DATA
                # MSG_DATA="VARNAME|DATA"

                elif info[0] == 'SENDMSG1' and info[2] == 'SND' and info[
                        3] == 'MSG' and info[4] == 'UNIQ' and info[
                            5] == self.client_id:
                    # verificar si info[4] aún está disponible
                    msg_key = ''
                    try:
                        # Buscar en directorio el idx o key del elemento con
                        # client_id
                        dest_id = info[6]
                        # Check assert if dest_id is in list

                        tot_length = c_length + \
                            len(dest_id) + len(info[5])
                        msg_data = self._input[tot_length::]
                        # GET DESTINY IDX (if existe give)
                        idx = LittleServerSession.get_idx(dest_id)
                        if idx != 'NONE':
                            # Create msg
                            pattern.replace('SND', 'RCV')
                            var_list.append(
                                pattern_value('[MSG_DATA]', msg_data))
                            var_list.append(
                                pattern_value('[DESTID]', dest_id))
                            var_list.append(
                                pattern_value("[CLIENTID]",
                                              self.client_id))
                            msg_send = fill_pattern(var_list, info_pt)
                            msg_key = 'snd_msg_ok'
                            LittleServerSession.send_info(idx,
                                                          msg_send)
                        else:
                            pattern.replace('SND', 'RCV')
                            msg = "\"Rechazado, no existe usuario\""
                            var_list.append(
                                pattern_value('[MSG_DATA]', msg))
                            var_list.append(
                                pattern_value('[DESTID]', dest_id))
                            var_list.append(
                                pattern_value("[CLIENTID]",
                                              self.client_id))
                            msg_send = fill_pattern(var_list, info_pt)
                            msg_key = 'snd_msg_reject'
                            LittleServerSession.send_info(self.idx,
                                                          msg_send)

                    except:
                        msg = '\"Ya no está conectado\"'
                        type(self).info_msg(self.idx, self.client_id, msg)
                        msg_key = 'snd_msg_nok'

                # SENDMSGALL
                # IDX|SND|MSG|ALL|CLID|MSG_DATA

                elif info[0] == 'SENDMSGALL' and info[2] == 'SND' and info[
                        3] == 'MSG' and info[4] == 'ALL' and info[
                            5] == self.client_id:
                    # verificar si info[5] aún está disponible
                    # Una lectura a la lista de sessiones y enviar por
                    # channel
                    tot_length = c_length + len(info[5])
                    msg_data = self._input[tot_length::]
                    msg_key = ''
                    pattern.replace('SND', 'RCV')
                    for k in LittleServerSession.SESSION.keys():
                        k_pattern = pattern
                        try:
                            # Crear msg: IDX RCV MSG DATA
                            idx = k
                            var_list.append(
                                pattern_value('[MSG_DATA]', msg_data))
                            var_list.append(
                                pattern_value("[CLIENTID]",
                                              self.client_id))
                            msg_send = fill_pattern(var_list, k_pattern)
                            LittleServerSession.send_info(idx,
                                                          msg_send)
                            msg_key = 'snd_all_ok_id'
                        except:
                            msg = '\"Ya no está conectado\"'
                            LittleServerSession.info_msg(
                                self.idx, self.client_id, msg)
                            msg_key = 'snd_all_nok_id'

                # SENDMSGGRP
                # IDX|SND|MSG|GRP|CLID|MSG_DATA

                elif info[0] == 'SENDMSGGRP' and info[2] == 'SND' and info[
                        3] == 'MSG' and info[4] == 'GRP' and info[
                            5] == self.client_id:
                    # verificar si info[4] aún está disponible
                    # Tengo una lista definida de estaciones a las que deseo
                    # enviar un msg
                    # Esta lista esta definida por un string a:b:c:d
                    s_list = info[6].split(":")
                    tot_length = c_length + len(info[5])
                    msg_data = self._input[tot_length::]
                    for s in s_list:
                        k_pattern = pattern
                        try:
                            client_id = s
                            idx = LittleServerSession.get_idx(
                                client_id)
                            pattern.replace('SND', 'RCV')
                            if idx != 'NONE':
                                var_list.append(
                                    pattern_value('[MSG_DATA]', msg_data))
                                var_list.append(
                                    pattern_value("[CLIENTID]", client_id))
                                var_list.append(
                                    pattern_value("[GRP_LST]", info[6]))
                                msg_send = fill_pattern(var_list,
                                                        k_pattern)
                                LittleServerSession.send_info(idx,
                                                              msg_send)
                                msg_key = 'snd_grp_ok_id'
                        except:
                            msg = '\"Ya no está conectado\"'
                            LittleServerSession.info_msg(
                                self.idx, self.client_id, msg)
                            msg_key = 'snd_grp_nok_id'

                elif info[0] == 'CLOSECNN' and self.group in type(
                        self).COMMANDS[info[0]]['group']:
                    idx = LittleServerSession.get_idx(client_id)
                    msg = '\"Cerrando conexión a solicitud de usuario admin\"'
                    LittleServerSession.info_msg(idx, self.client_id,
                                                 msg)
                    chann = type(self).CHANNEL[idx]
                    chann.close()
                    msg_key = 'session_close'

                else:
                    print(info)
                    msg = '\"Tu comando no está completo\"'
                    LittleServerSession.info_msg(self.idx, self.client_id,
                                                 msg)

            else:
                print(info)
                msg = '\"Tu comando no se reconoce\"'
                LittleServerSession.info_msg(self.idx, self.client_id,
                                             msg)

        else:
            print(info)
            msg = '\"Tu mensaje no se reconoce\"'
            print(msg)
            print(self.idx)
            print([self.idx, self.client_id, msg])
            LittleServerSession.info_msg(self.idx, self.client_id, msg)

    def eof_received(self):
        """
        End of session
        :param self:
        :return:
        """
        self._chan.write('Total = %s\r\n' % self._total)
        self._chan.exit(0)

    def get_id(self):
        """
        Return client id
        :param self:
        :return:
        """
        return self.client_id

    def connection_lost(self, exc):
        """
        Erase chan and client from lists
        :param self:
        :param exc:
        :return:
        """
        method = 'connection_lost'
        print("Conexión perdida")
        print(self._chan)

    @staticmethod
    def get_idx(client_id):
        """
        Return IDX, session id, when the input it's a correct client id
        :param client_id:
        :return:
        """
        method = 'get_idx'
        W = LittleServerSession.SESSION
        idx = 'NONE'
        for k, c in W.items():
            if c['id_cliente'] == client_id:
                idx = k
                # podria ser mismo cliente con varias sesiones?
                # manejar lista idx:futuro
                break
        msg_key = "idx_fclient"
        LittleServerSession.addLog(method, idx, msg_key, client_id)  # info
        return idx

    @staticmethod
    def send_info(chan_id, info):
        """
        Send info using a channel
        :param chan_id:
        :param info:
        :return:
        """
        method = 'send_info'
        msg_key = ''
        print("En sendinfo")
        print(chan_id)
        try:
            if chan_id in LittleServerSession.ANNON_CHANNEL:
                chan = LittleServerSession.ANNON_CHANNEL[chan_id]
            elif chan_id in LittleServerSession.CHANNEL:
                chan = LittleServerSession.CHANNEL[chan_id]
            else:
                print("No channel in list")

            if info is not '':
                try:
                    chan.write(info + m_end)
                except Exception as exec:
                    print(exec)
            msg_key = 'snd_msg_ok'
        except Exception as exec:
            print(exec)
            print("Error en enviar información")
            msg_key = 'snd_msg_nok'
        try:
            print("Chan log:")
            print(chan.get_extra_info("peername"))
            # LittleServerSession.addLog(method, chan_id, msg_key,
            #"" info)  #
            #msgj = "Log ok" + m_end

           # chan.write(msgj)
            # print(msgj)
            print(chan.get_extra_info("peername"))

        except Exception as exec:
            print("Error log")
            print(exec)

    @staticmethod
    def info_msg(idx, client_id, msg):
        """
        Send a info msg to idx session
        :param idx: session identificator (string)
        :param client_id: client identificator (string)
        :param  msg: message (string)
        """
        var_list = []
        msg_key = ''
        print("INFO MSG")
        # LittleServerSession.send_info(idx, "Recibe esto %%%")
        info = LittleServerSession.COMMANDS['INFO']['answer']
        var_list.append(pattern_value("[MSG_DATA]", msg))
        var_list.append(pattern_value("[CLIENTID]", client_id))
        msg_send = fill_pattern(var_list, info)
        print(idx)
        print("Enviando a terminal: " + msg_send)
        try:
            if msg_send is not '':
                print("To send_info")
                LittleServerSession.send_info(idx, msg_send)
                msg_key = 'info_sended'
        except Exception as exec:
            print(exec)
            print("Info no se pudo enviar desde info_msg")
            print("PRRPRPRP")
            msg_key = 'no_info_sended'

        gns_method = 'send_info'

    @staticmethod
    def add_session(idx, id_cl, group, chan, ip, port):
        """
        Add session to the SESSION and CHANNEL dicts

        :param idx: session id assigned by server, string value
        :param id_cl: client id gived by client, string value
        :param chan: channel instance,
        :param ip: ip from client, string value
        :param port: port from client, int value
        :return:
        """

        client_id = idx
        gns_method = 'add_session'

        print([idx, id_cl, group, chan, ip, port])

        client = dict(
            id_cliente=id_cl,
            group=group,
            ip=ip,
            port=port)
        msk_key = ''
        # print("ANNON")
        # print(LittleServerSession.ANNON_CHANNEL)
        # print("OFFICIAL")
        # print(LittleServerSession.CHANNEL)
        try:
            LittleServerSession.ANNON_CHANNEL.pop(client_id)
            LittleServerSession.SESSION.update({client_id: client})
            print(chan)
            LittleServerSession.CHANNEL.update({client_id: chan})
            print(LittleServerSession.CHANNEL)
            msg_key = 'client_added'
        except:
            msg_key = 'client_not_added'
        #print("Cliente añadido a sesion " + msg_key)
        #client_list = "" + json.dumps(client)
        # print(client_list)
        # print("Añadido")
        # print(LittleServerSession.sessions())

    @staticmethod
    def sessions():
        """
        Return sessions
        :return:
        """
        method = 'sessions'
        return LittleServerSession.SESSION

    @staticmethod
    def commands_by_group(group):
        """
        Returns the command list filtered by allowed group
        :param group: string name
        :return:
        """
        out_group = ''
        try:
            all_var = LittleServerSession.COMMANDS
            out_group = dict()
            for k in all_var.keys():
                if group in all_var[k]['group']:
                    out_group.update({k: all_var[k]})
        except Exception as exec:
            print(exec)
        return out_group


class LittleServer(asyncssh.SSHServer):

    def session_requested(self):
        return LittleServerSession()

import asyncio
import sys
import os

try:
    from .conf.settings import PORT, HOST_KEYS, CLIENT_KEYS
except Exception:
    from conf.settings import PORT, HOST_KEYS, CLIENT_KEYS

async def start_server():
    """
    That method run a server to atend clients, using asyncio coroutines
    :return:
    """
    server = await asyncssh.create_server(LittleServer, '', PORT,
                                          server_host_keys=HOST_KEYS,
                                          authorized_client_keys=CLIENT_KEYS)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()

    try:
        """
        Run server in a loop
        """
        loop.run_until_complete(start_server())
    except (OSError, asyncssh.Error) as exc:
        sys.exit('Error starting server: ' + str(exc))

    loop.run_forever()
