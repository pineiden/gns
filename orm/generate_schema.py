from sqlalchemy import MetaData
from sqlalchemy_schemadisplay import create_schema_graph
from db_session import eng_path

# Graph with pydot

graph = create_schema_graph(
    metadata=MetaData(eng_path),
    show_datatypes=True,
    show_indexes=True,
    rankdir='LR',
    concentrate=False)

graph.write_png('gns_schema.png')
