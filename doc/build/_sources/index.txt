.. GPS Network Server documentation master file, created by
   sphinx-quickstart on Wed Jul  6 11:17:29 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenid@ a la documentación de GPS Network Server!
====================================================

Con el sistema GNS se provee un servicio de gestión de mensajes entre una red
de servidores que gestionan la adquisición de los datos de las estaciones GPS.

Esto permitiría gestionar diferentes protocolos de comunicación con que 
trabaje alguna estación determinada.

Contenidos:

.. toctree::
   :maxdepth: 2
   
   files/intro
   files/models
   files/server
   files/daemon



Índices and tablas
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

