# enable asyncio items and systems
import asyncio
import asyncssh
import logging
import datetime
import json
try:
    from orm.db_session import session
    from orm.models import Level, User, Command, Alert, Message, Log
    from orm.db_users import SessionGNS
    from orm.password import PasswordHash
except Exception:
    from .orm.db_session import session
    from .orm.models import Level, User, Command, Alert, Message, Log
    from .orm.db_users import SessionGNS
    from .orm.password import PasswordHash

try:
    from conf.settings import LIST_COMMANDS, LIST_LOGS, separator
except Exception:
    from .conf.settings import LIST_COMMANDS, LIST_LOGS, separator
"""
Este método es para generar un identificador único para distintas instancias
durante el tiempo de operación del servidor
"""
try:
    from library import my_random_string, fill_pattern,\
        pattern_value, context_split
except Exception:
    from .library import my_random_string, fill_pattern,\
        pattern_value, context_split


class GPSNetworkServerSession(asyncssh.SSHServerSession):
    """A class to manage session from a client connection.
    It define an ID by session: IDX
    .. note::
        [X] parenthesis mean a variable value of X

    When a client it's connected send a string  <IDX>|SND|ID to it,
    in what the client has to synchronice the session. The client must take IDX
    and send to server its *client id*:

    * **msg** [IDX]|ID|[CLIENTID].

    The client can ask or make a query about the another clients, getting the
    list:

    * **msg** [IDX]|GET|LST|[CLIENTID], the server return dictionary (hash)
        with the information

    The others commands could be obtained asking about COMMAND dict.

    * **msg** [IDX]|GET|LST|CMD|[CLIENTID].

    And to end a message to some client:

    * **1:1** -> [IDX]|SND|MSG|UNIQ|CLID|MYID|MSG_DATA

    * **1:all** -> [IDX]|SND|MSG|ALL|[CLIENTID]|[MSG_DATA]

    * **1:n<=all** -> [IDX]|SND|MSG|GRP|[CLID]|[MSG_DATA]

    """
    SESSION = {}
    ANNON_CHANNEL = {}
    CHANNEL = {}
    COMMANDS = LIST_COMMANDS
    log_file = 'log/ssh_session.log'
    logging.basicConfig(filename=log_file, level=logging.DEBUG)
    LOG_MSG = LIST_LOGS
    # identificador de session:
    # Debe contener una lista de sesiones
    idx = []

    def __init__(self, uin=6):
        """
        Init:

        :param uin: session identifier length
        """
        print("Se inicia GNS Server")
        self.db_session = SessionGNS()
        self.gns_method = 'init'
        self._input = ''
        self._total = 0
        self.uin = uin
        IDX = my_random_string(self.uin)
        self.idx = IDX
        self.group = 'all'
        self.client_id = 'ANNON'
        # print (IDX)
        t = True
        while t:
            if IDX not in type(self).idx:
                type(self).idx.append(IDX)
                t = False
            else:
                IDX = my_random_string(self.uin)
        print(self.idx)
        # -> addlog largo id
        msg_key = 'set_uin'
        # info Esto
        # está bien si y solo si en la creacion de nuevas instancias no se
        # modifica el uin de otra manera sería necesario reiniciar servidor
        print("Calculando tamaños")
        for k in GPSNetworkServerSession.COMMANDS.keys():
            # print(k)
            c_length = len(k)
            plecas = GPSNetworkServerSession.COMMANDS[k]['code'].count(
                separator)
            if k == 'SYNCCLIENT':
                c_length += self.uin + plecas + 2  # ID
            elif k == 'CLNTPSSWD':
                c_length += self.uin + plecas + 2  # PW
            elif k == 'INFO':
                c_length += self.uin + plecas + 0  # no names
            elif k == 'SESSIONLST':
                c_length += self.uin + plecas + 3 + 3  # GEt+LST
            elif k == 'COMMANDLST':
                c_length += self.uin + plecas + 3 + 3 + 3  # GET+LST+CMD
            elif k == 'SENDMSG1':
                c_length += self.uin + plecas + 3 + 3 + 4  # SND+MSG+UNIQ
            elif k == 'SENDMSGALL':
                c_length += self.uin + plecas + 3 + 3 + 3  # SND+MSG+ALL
            elif k == 'SENDMSGGRP':
                c_length += self.uin + plecas + 3 + 3 + 3  # SND+MSG+GRP
            GPSNetworkServerSession.COMMANDS[k]['c_length'] = c_length
        # print("End-init")
        print(self.gns_method + " " + self.idx + " " + msg_key + " " + str(
            self.uin))
        try:
            print(self.gns_method)
            print(self.idx)
            print(msg_key)
            print(str(self.uin))
            self(type).addLog(self.gns_method, self.idx, msg_key,
                              str(self.uin))
        except:
            print("No se pudo registrar en log inicio de sesión")

    def connection_made(self, chan):
        """
        It run when the connection it's ok.
        It allow to send a msg to client asking the id

        :param chan: A Channel
        :return:
        """
        # called when channel is openned sucessfully SSHClientChannel
        print("Connection made")
        print(chan.get_extra_info('peername'))
        try:
            chan.write("Conexión exitosa")
        except:
            print("No se envio exito conexion")
        method = 'connection_made'
        self._chan = chan
        GPSNetworkServerSession.ANNON_CHANNEL.update({self.idx: chan})

        # print (self._chan.get_extra_info ('connection'))
        # print (self._chan.get_extra_info ('local_peername'))
        # print (self._chan.get_extra_info ('remote_peername'))
        # print (self._chan.get_extra_info ('connection').get_extra_info (
        #    'send_mac'))
        # rint(self._chan)
        print("Serversession")
        # id_cl = len(GPSNetworkServerSession.clients())

        (ip, port) = chan.get_extra_info('peername')
        self.ip = ip
        self.port = port
        IDX = self.idx
        msg = "\"Tu ip es: " + ip + " " + self.client_id + "\""
        type(self).info_msg(IDX, self.client_id, msg)
        msg = "\"INFO|" + IDX + "|SND|ID \""
        try:
            chan.write(msg)
            msg_key = 'snd_id_ok'
        except:
            msg_key = 'snd_id_nok'

    def shell_requested(self):
        """
        Shell enabled
        :return:
        """
        return True

    def exec_requested(self, command):
        """run SSH command"""
        return True

    def pty_requested(self, term_type, term_size, term_modes):
        return True

    def session_started(self):
        """
        Mensaje de inicio de sesion
        :return:
        """
        method = 'session_started'
        msg_key = ''
        try:
            print("Iniciando sessión")
            self._chan.write("Conectando")
            type(self).info_msg(self.idx, self.client_id,
                                "\"Bienvenid@ al servidor GNS\"")
            type(self).info_msg(
                self.idx, self.client_id,
                "\"Obtén tus comandos: COMMANDLST | [IDX] | GET | LST | CMD |"
                + " [CLIENTID]\r\n Donde CLIENTID:ANNON\"")
            msg_key = 'wc_ok'
        except:
            msg_key = 'wc_nok'

    def data_received(self, data, datatype):
        """
        Data proccesing, here the data can be send and received
        :param data: is a string or bytes received from a client
        :param datatype: not used in that case
        :return:
        """
        method = 'data_received'
        var_list = []
        self._input += data.strip()
        info = self._input.split('|')
        print(info)
        print("Recibiendo data:")
        print("Nombre de Comando:")
        print(info[0])

# Create alias Session
GNSs = GPSNetworkServerSession


class GPSNetworkServer(asyncssh.SSHServer):
    """It allow a system to attend clients, receive and send messages. The
    messages are recognized by its structure, that have commands inside to
    route or obtain some information. You can define a level permissions
    categories (groups) and assign to the users Some can do all commands,
    another can do a few commands The groups are defined on the database. The
    log messages, too. Use SSH protocol to establish a secure conedtion beetwen
    pairs

    """

    def session_requested(self):
        """
        Generate the session using GNSs

        :return:
        """
        # print ("Sesión: ")
        self.session = GPSNetworkServerSession()
        # MySSHServer.add_session(self.id_cl,self.session,self.ip,self.port)
        # print (self.session)
        # print(self.session._chan)
        # print(self.session.client_id)
        return self.session

    def connection_made(self, connection):
        """
        When connection it's made, run code

        :param connection:
        :return:
        """
        self.connection = connection
        (ip, port) = connection.get_extra_info('peername')
        self.ip = ip
        self.port = port


GNS = GPSNetworkServer
