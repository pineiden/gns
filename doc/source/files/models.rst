.. _models:

===========
Modelos GNS
===========

Estos modelos establecen las tablas de datos necesarias para
controlar el sistema

Se utiliza SqlAlchemy para la creación del esquema y la generación
de los modelos python.

Considera:

#. Niveles (ver :ref:`intro`)
	
	{0 Administrador,1 Monitor,2 Cliente}
	
	* nivel (int)
	* grupo
	* descripción
	
.. figure:: img/niveles.svg
   :width: 100 %	
	
#. Usuarios cliente
	* nombre
	* pass (sha256)
	* grupo -> Niveles:ID_grupo
	
#. Mensajes de registro
	* code
	* descripción
	* nivel_alerta
	
#. Comandos permitidos
	* nombre
	* descripción
	* comando
	* nivel

#. Log
	* timestamp
	* method
	* mensaje -> Mensajes
	* valor (input causante)
