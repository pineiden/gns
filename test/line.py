class line(object):
    __c = 0
    __lines = 0
    def __init__(self, a = 0, *args, **kwargs):
        self.a=a
        type(self).__lines +=1

    def set_a(self, a):
        self.a=a
    
    def y(self, x):
        self.y = self.a*x+type(self).__c
        return self.y

    @staticmethod
    def line_c():
        return line.__c
    
    @staticmethod
    def lines():
        return line.__lines


if __name__=="__main__":
    print(line.lines())
    y=line(a=3)
    print(y.lines())
    z=line(a=1)
    print(z.lines())
    print("desde mismo objeto")
    print(line.lines())
    
