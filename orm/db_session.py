from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

eng_path = 'sqlite:///server.db'
engine = create_engine(eng_path)
connection = engine.connect()
Session = sessionmaker(bind=engine)

session = Session()
