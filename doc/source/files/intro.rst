.. _intro:

============
Introducción
============

Este módulo permite establecer una red de servidores que permitan la adquisición 
de datos desde las estaciones GPS instaladas.

Tiene como motivo de diseño el ser un gestor de las comunicaciones entre
terminales de servidores.

Hay tres roles determinados, con sus funcionalidades caracterizadas:

* **Monitor** : Administra la información de estaciones y asigna conexiones, además de controlar su adquisición. 
* **Administrador** : Es el único rol que puede agregar, quitar, encender-apagar los dispositivos del sistema
* **Cliente** : Recibe las ordenes desde el servidor, agrega estaciones y las gestiona, tanto su conexión como sus datos.

El sistema integra un modulo asyncio, una base de datos con el registro de clientes y logging


