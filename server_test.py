import asyncio
import asyncssh
import sys


class MySSHServerSession(asyncssh.SSHServerSession):

    def __init__(self):
        self._input = ''
        self._total = 0

    def connection_made(self, chan):
        self._chan = chan

    def shell_requested(self):
        return True

    def session_started(self):
        self._chan.write('Enter numbers one per line, or EOF when done:\n')

    def data_received(self, data, datatype):
        self._input += data

        lines = self._input.split('\n')
        for line in lines[:-1]:
            try:
                if line:
                    self._total += int(line)
            except ValueError:
                self._chan.write_stderr('Invalid number: %s\n' % line)

        self._input = lines[-1]

    def eof_received(self):
        self._chan.write('Total = %s\n' % self._total)
        self._chan.exit(0)

    def break_received(self, msec):
        self.eof_received()


class MySSHServer(asyncssh.SSHServer):

    def session_requested(self):
        return MySSHServerSession()

async def start_server():
    await asyncssh.create_server(MySSHServer, '', 8022,
                                 server_host_keys=[
                                     '/etc/ssh/ssh_host_dsa_key'],
                                 authorized_client_keys='/home/david/.ssh/authorized_keys')

loop = asyncio.get_event_loop()

try:
    loop.run_until_complete(start_server())
except (OSError, asyncssh.Error) as exc:
    sys.exit('Error starting server: ' + str(exc))

loop.run_forever()
